package academy.learnprogramming;

public interface IMessageGenerator {

    String getMainMessage();

    String getResultMessage();

}
