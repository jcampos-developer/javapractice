package academy.learnprogramming.console;

import academy.learnprogramming.AppConfig;
import academy.learnprogramming.Game;
import academy.learnprogramming.MessageGenerator;
import academy.learnprogramming.NumberCheck;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

    private static final Logger log = LoggerFactory.getLogger(Main.class);

    //private static final String CONFIG_LOCATION = "beans.xml";


    public static void main(String[] args) {
        log.info("Guess the number game");

        NumberCheck isOdd = (x, y) -> x % y == 1;
        NumberCheck plus = (x,y) -> x + y == x;

        ConfigurableApplicationContext context =
                new AnnotationConfigApplicationContext(AppConfig.class);

        Game game = context.getBean(Game.class);

        MessageGenerator messageGenerator = context.getBean(MessageGenerator.class);

        game.setGuess(5);

        log.info("guess has beet set to {}", game.getGuess());

        log .info(messageGenerator.getMainMessage());
        log.info(messageGenerator.getResultMessage());

        context.close();
    }




}
