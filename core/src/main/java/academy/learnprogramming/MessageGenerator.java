package academy.learnprogramming;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

public class MessageGenerator implements IMessageGenerator {

    private static final Logger log = LoggerFactory.getLogger(MessageGenerator.class);

    @Autowired
    private Game game;

    private int guessCount = 10;

    @Override
    public String getMainMessage() {
        return "Number is in between " + game.getSmallest() + " and " + game.getBiggest() + "";
    }

    @Override
    public String getResultMessage() {
        String res;
        if (game.isGameWon()) {
            return "Yay! You won!";
        } else if (game.isGameLost()) {
            return ":( I am sorry but you lost";
        } else if (!game.isValidNumberRange()) {
            return "Invalid number range";
        } else if (game.getRemainingGuesses() == guessCount) {
            return "What is your first choice?";
        } else {
            String direction = "Lower";

            if (game.getGuess() < game.getNumber()) {
                direction = "Higher";
            }

            return direction + ". You have" + game.getRemainingGuesses() + " remaining." ;
        }
    }

    @PostConstruct
    private void logMethod() {
        log.info("Message Generator successfully generated");
    }

}
