package academy.learnprogramming;

public interface INumberGenerator {

    int next();

    int getMaxNumber();

}
