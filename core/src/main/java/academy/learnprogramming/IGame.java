package academy.learnprogramming;

public interface IGame {

    int getNumber();

//    void setGuesses(int guess);
//
//    int getGuesses();

    int getSmallest();

    int getBiggest();

    int getRemainingGuesses();

    void reset();

    void check();

    boolean isValidNumberRange();

    boolean isGameWon();

    boolean isGameLost();

}


