package academy.learnprogramming;

@FunctionalInterface
public interface NumberCheck {
    boolean check(int x, int y);
}
