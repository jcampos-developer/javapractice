package academy.learnprogramming;

import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class Game implements IGame {

    private static final Logger log = LoggerFactory.getLogger(Game.class);
    // -- fields --
    @Autowired
    private NumberGenerator numberGenerator;

    private int guessCount = 10;

    @Getter
    @Setter
    private int guess;

    private int number;

    private int smallest;

    private int biggest;

    private int remainingGuesses;

    private boolean validNumberRange = true;
    // -- fields --
    @PostConstruct
    @Override
    public void reset() {
        smallest = 0;
        guess = 0;
        biggest = numberGenerator.getMaxNumber();
        remainingGuesses = guessCount;
        number = numberGenerator.next();
        log.info("The number is {}", number);
    }

    @PreDestroy
    public void endGame() {
        log.info("Destroying game");
    }

    @Override
    public int getNumber() {
        return number;
    }

//    @Override
//    public void setGuesses(int guess) {
//        guessCount = guess;
//    }
//
//    @Override
//    public int getGuesses() {
//        return guess;
//    }

    @Override
    public int getSmallest() {
        return smallest;
    }

    @Override
    public int getBiggest() {
        return biggest;
    }

    @Override
    public int getRemainingGuesses() {
        return remainingGuesses;
    }

    @Override
    public void check() {
        checkValidNumberRange();

        if (isValidNumberRange()) {
            if (guess > number) {
                biggest = guess - 1;
                remainingGuesses--;
            } else if (guess < number) {
                smallest = guess + 1;
                remainingGuesses--;
            } //else {
                // won game
            //}
        }
    }

    @Override
    public boolean isValidNumberRange() {
        return validNumberRange;
    }

    @Override
    public boolean isGameWon() {
        return guess == number;
    }

    @Override
    public boolean isGameLost() {
        return !isGameWon() && remainingGuesses <= 0;
    }

    private void checkValidNumberRange() {
        validNumberRange = (guess >= smallest) && (guess <= biggest);
    }
}
